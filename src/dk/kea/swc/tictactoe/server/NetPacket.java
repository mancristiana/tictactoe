package dk.kea.swc.tictactoe.server;

import java.io.Serializable;

/**
 * Created by Cris on 11-Oct-15.
 */
public class NetPacket implements Serializable {

    private String sender;
    private String receiver;
    private String header;
    private Object content;

    public NetPacket() {
    }

    public NetPacket(String receiver, String header) {
        this.receiver = receiver;
        this.header = header;
    }

    public NetPacket(Object content, String header, String receiver, String sender) {
        this.content = content;
        this.header = header;
        this.receiver = receiver;
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}
