package dk.kea.swc.tictactoe.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Server {

    private static ServerSocket serverSocket;
    private static Map<String, ObjectOutputStream> clientMap;
    private static Object logLock;

    public static void startServer(int port) throws IOException {
        logLock = new Object();
        clientMap = new HashMap<>();
        serverSocket = new ServerSocket(port);
        Server.log("Server started");
    }

    public static void main(String[] args) throws IOException {
        Server.startServer(7324);

        while (true) {
            Socket client = serverSocket.accept();
            Server.log(client.toString() + " connected.");
            Server.serve(client);
            Server.log(client.toString() + " is being served." + '\n');
        }
    }

    private static void serve(Socket client) {
        new Thread(() -> {
            String clientName = null;
            ObjectInputStream inFromClient = null;
            ObjectOutputStream outToClient = null;
            try {
                while (true) {
                    if (inFromClient == null)
                        inFromClient = new ObjectInputStream(client.getInputStream());

                    NetPacket packet = (NetPacket) inFromClient.readObject();

                    Server.log("Received: " + packet.getHeader());
                    Server.log("From: " + client.getInetAddress().getHostAddress() + ":" + client.getPort());
                    Server.log("To: " + packet.getReceiver() + '\n');

                    ObjectOutputStream receiverStream;

                    if (packet.getReceiver().equals("Server")) {
                        clientName = packet.getHeader();
                        packet.setSender("Server");
                        packet.setReceiver(clientName);

                        if (outToClient == null)
                            outToClient = new ObjectOutputStream(client.getOutputStream());

                        if (clientMap.get(clientName) == null) {
                            packet.setHeader("Success");
                            clientMap.put(clientName, outToClient);
                        } else {
                            packet.setHeader("Name Unavailable");
                        }

                        receiverStream = outToClient;
                    } else {
                        packet.setSender(clientName);
                        receiverStream = clientMap.get(packet.getReceiver());
                    }

                    synchronized (receiverStream) {
                        receiverStream.writeObject(packet);
                        receiverStream.flush();
                    }

                    Server.log("Sent: " + packet.getHeader());
                    Server.log("From: " + clientName);
                    Server.log("To: " + packet.getReceiver() + '\n');
                }
            } catch (Exception e) {
                clientMap.remove(clientName);

                e.printStackTrace();
                Server.log(e.getMessage() + " for: " + clientName);
                Server.log("Nr of players: " + clientMap.size() + '\n');
            }
        }).start();
    }

    private static void log(String message) {
        synchronized (logLock) {
            System.out.println(message);
        }
    }
}