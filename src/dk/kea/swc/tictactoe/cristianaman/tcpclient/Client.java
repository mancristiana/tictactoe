package dk.kea.swc.tictactoe.cristianaman.tcpclient;

import dk.kea.swc.tictactoe.server.NetPacket;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Created by Cris on 11-Oct-15.
 */
public class Client {
    private static final String serverAddress   = "theruffled.me";
    private static final int    serverPort      =  7324;
    private static Socket socket;
    private static ObjectInputStream input;
    private static ObjectOutputStream output;

    private static void createSocket() throws Exception {
        if(socket == null) {
            socket = new Socket(serverAddress, serverPort);
        }
    }

    public static void sendToServer(NetPacket packet) throws Exception {
        Client.createSocket();
        if (output == null) // This prevents Corrupt header exception, also we don't create a new output stream everytime
            output = new ObjectOutputStream(socket.getOutputStream());

        output.writeObject(packet);

        System.out.println("Sent: " + packet.getHeader() + "\nTo: " + packet.getReceiver() + "\n");
    }

    public static NetPacket receiveFromServer() throws Exception {
        Client.createSocket();
        if (input == null)
            input = new ObjectInputStream(socket.getInputStream());

        NetPacket packet = (NetPacket) input.readObject();

        System.out.println("Received: " + packet.getHeader() + "\nFrom: " + packet.getSender() + "\n");
        return packet;
    }
}
