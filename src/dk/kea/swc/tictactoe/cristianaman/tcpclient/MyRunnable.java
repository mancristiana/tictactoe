package dk.kea.swc.tictactoe.cristianaman.tcpclient;

/**
 * Created by Cris on 11-Oct-15.
 */
public interface MyRunnable {
    public void run() throws Exception;
}
