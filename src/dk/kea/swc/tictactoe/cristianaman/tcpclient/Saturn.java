package dk.kea.swc.tictactoe.cristianaman.tcpclient;

import dk.kea.swc.tictactoe.cristianaman.Game;
import dk.kea.swc.tictactoe.cristianaman.GameController;
import dk.kea.swc.tictactoe.cristianaman.Main;
import dk.kea.swc.tictactoe.cristianaman.StartController;
import dk.kea.swc.tictactoe.server.NetPacket;
import javafx.application.Platform;
import javafx.scene.control.Alert;

/**
 * Created by Cris on 11-Oct-15.
 */
public class Saturn {
    private static String playerName = "";
    private static String opponentName = "";

    private final static String PROTOCOL_SERVER_SUCESS = "Success";
    private final static String PROTOCOL_SERVER_UNAVAILABLE = "Name Unavailable";
    private static boolean isMyNameInServer = false;

    private final static String PROTOCOL_INVITE = "I wanna play";
    private final static String PROTOCOL_ACCEPT_INVITE = "I wanna play as well";
    private final static String PROTOCOL_POSITION = "Position:";
    private final static String PROTOCOL_SAVE = "Saved the game for later";
    private final static String PROTOCOL_LOAD = "Savegame";


    public static boolean sendMyNameToServer() throws Exception {
        // Set my name in the server's map
        NetPacket helloServer = new NetPacket(null, playerName, "Server", playerName);
        System.out.println(playerName);
        Client.sendToServer(helloServer);

        // Receive a confirmation
        NetPacket success = Client.receiveFromServer();
        while (!success.getSender().equals("Server"))
            success = Client.receiveFromServer();
        if (success.getHeader().equals(PROTOCOL_SERVER_UNAVAILABLE)) {
            Platform.runLater(() -> {
                StartController.showAlert(Alert.AlertType.ERROR, "Your name is not available!\nPlease be more original!");
                Main.getStartController().getYouNameField().setEditable(true);
            });
        } else {
            isMyNameInServer = true;
        }
        return isMyNameInServer;
    }

    public static void sendInvitation() {
        runInParallel(() -> {
            // Set my name in the server's map
            if(!isMyNameInServer) {
                boolean success = sendMyNameToServer();
                if(!success)
                    return;
            }

            // Send an invitation
            NetPacket invitation = new NetPacket(null, PROTOCOL_INVITE, opponentName, playerName);
            Client.sendToServer(invitation);

            // Receive a confirmation
            NetPacket confirmation = Client.receiveFromServer();
            while(!(confirmation.getSender().equals(opponentName)||confirmation.getHeader().equals(PROTOCOL_ACCEPT_INVITE)))
                confirmation = Client.receiveFromServer();

            // On the main thread
            Platform.runLater(() -> {
                Game.setStatus(Game.STATUS_STARTED);
                Main.loadGame();
            });
        });
    }

    public static void waitForInvitation() {
        runInParallel(() -> {
            // Set my name in the server's map
            if(!isMyNameInServer) {
                boolean success = sendMyNameToServer();
                if(!success)
                    return;
            }

            // Wait for an invitation
            NetPacket packet = Client.receiveFromServer();
            while(!packet.getHeader().equals(PROTOCOL_INVITE))
                packet = Client.receiveFromServer();

            // Save opponent who sent invitation
            setOpponentName(packet.getSender());

            // Send a confirmation
            NetPacket confirmation = new NetPacket(null, PROTOCOL_ACCEPT_INVITE, opponentName, playerName);
            Client.sendToServer(confirmation);

            // On the main thread
            Platform.runLater(() -> {
                Game.setStatus(Game.STATUS_STARTED);
                Main.loadGame();
            });

            receivePosition();
        });

    }

    public static void sendPosition(int x, int y) {
        runInParallel(() -> {
            NetPacket myMove = new NetPacket(null, PROTOCOL_POSITION + x + "" + y, opponentName, playerName);
            Client.sendToServer(myMove);

            // Receive a response if game is not over
            if (!Game.isGameOver()) {
                receivePosition();
            }
        });
    }

    private static void receivePosition() throws Exception {
        NetPacket opponentMove = Client.receiveFromServer();
        while (!isValidMove(opponentMove))
            opponentMove = Client.receiveFromServer();

        // Process move
        String validMove = opponentMove.getHeader();
        if(validMove.contains(PROTOCOL_POSITION)) {
            validMove = validMove.replace(PROTOCOL_POSITION, "");
            System.out.println(validMove);
            int opponentX = Integer.parseInt(validMove.substring(0, 1));
            int opponentY = Integer.parseInt(validMove.substring(1, 2));

            // On the main thread
            Platform.runLater(() -> {
                Main.getGameController().handleMove(opponentX, opponentY, false);
            });
        } else if(validMove.contains(PROTOCOL_SAVE)){
            Platform.runLater(() -> {
                Main.getGameController().showAlert(Alert.AlertType.INFORMATION, "Your opponent saved the game and left...\n:( :( :(\nIt's okay... you can start a new game");
                Main.getGameController().handleNew();
            });
        } else if(validMove.contains(PROTOCOL_LOAD)) {
            // LOAD GAME
            byte[][] board = (byte[][]) opponentMove.getContent();
            Platform.runLater(() -> {
                Game.setBoard(board);
                Main.getGameController().handleLoad();
                Main.getGameController().showAlert(Alert.AlertType.INFORMATION, "The last game has been loaded :)");

            });
            receivePosition();
        }
    }

    private static boolean isValidMove(NetPacket opponentMove) {
        // Make sure your opponent is the one sendind a packet
        if(!opponentMove.getSender().equals(opponentName))
            return false;
        // If it contains expected protocol return true
        // Position
        if(opponentMove.getHeader().contains(PROTOCOL_POSITION))
            return true;
        // Load a game
        if(opponentMove.getHeader().contains(PROTOCOL_LOAD))
            return true;
        // Save game for later
        if(opponentMove.getHeader().contains(PROTOCOL_SAVE))
            return true;
        return false;
    }

    public static void runInParallel(MyRunnable block) {
        new Thread(() -> {
            try {
                block.run();
            } catch (Exception e) {
                Platform.runLater(() -> {
                    GameController.showAlert(Alert.AlertType.ERROR, "Something went wrong :'(.\nThe app will close.");
                    e.printStackTrace();
                    System.exit(-1);
                });
            }
        }).start();
    }

    public static String getPlayerName() {
        return playerName;
    }

    public static void setPlayerName(String playerName) {
        Saturn.playerName = playerName;
    }

    public static String getOpponentName() {
        return opponentName;
    }

    public static void setOpponentName(String opponentName) {
        Saturn.opponentName = opponentName;
    }

    public static boolean isMyNameInServer() {
        return isMyNameInServer;
    }
}
