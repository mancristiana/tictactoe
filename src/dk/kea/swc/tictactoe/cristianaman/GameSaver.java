package dk.kea.swc.tictactoe.cristianaman;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Created by Cris on 12-Oct-15.
 */
public class GameSaver {
    private static String fileName = "saved";

    public static byte[][]  loadGame() throws FileNotFoundException {
        byte[][] savedBoard = new byte[3][3];

        Scanner input = new Scanner(new File(fileName + ".txt"));
        boolean ok = input.hasNextInt();
        for(int i = 0; i < 3 && ok; i++)
            for(int j = 0; j < 3 && ok; j++) {
                savedBoard[i][j] = input.nextByte();
                ok = input.hasNextInt();
            }

        input.close();

        return savedBoard;
    }

    public static void saveGame(byte[][] currentBoard) throws FileNotFoundException {
        PrintStream output 	= new PrintStream( new File(fileName + ".txt") );

        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++) {
                output.println(currentBoard[i][j]); //print it to the File
            }
        output.close();
    }
}
