package dk.kea.swc.tictactoe.cristianaman;

/**
 * Created by Cris on 01-Oct-15.
 */
public class Game {
    private static final byte X_VALUE = 1;
    private static final byte O_VALUE = -1;

    private static byte[][] board = new byte[3][3];
    private static int moveCount;
    private static boolean isMyMoveX;
    private static boolean isGameOver;
    private static String gameStatus;

    public static final String STATUS_NEW       = "New game";
    public static final String STATUS_STARTED   = "Game has started!";
    public static final String STATUS_X_WON     = "X won!";
    public static final String STATUS_O_WON     = "O won!";
    public static final String STATUS_TIE       = "It is a tie!";

    static {
        gameStatus = STATUS_NEW;
        moveCount = 1;
        isMyMoveX = false;
        isGameOver = false;
    }

    public static void makeMove(int x, int y) {
        board[x][y] = (isTurnX()) ? X_VALUE : O_VALUE;
        updateStatus();
        moveCount++;
    }

    private static void updateStatus() {
        if(moveCount >= 5) {
            // We calculate sums on each line to check if game over
            int[] sums = new int[8];
            // check if line on diagonals
            sums[0] = board[0][0] + board[1][1] + board[2][2];
            sums[1] = board[2][0] + board[1][1] + board[0][2];
            // check if line on rows
            sums[2] = board[0][0] + board[0][1] + board[0][2];
            sums[3] = board[1][0] + board[1][1] + board[1][2];
            sums[4] = board[2][0] + board[2][1] + board[2][2];
            // check if line on columns
            sums[5] = board[0][0] + board[1][0] + board[2][0];
            sums[6] = board[0][1] + board[1][1] + board[2][1];
            sums[7] = board[0][2] + board[1][2] + board[2][2];

            for(int i = 0; i < 8 && !isGameOver; i++) {
                isGameOver = true;
                if(sums[i] == -3)       gameStatus = STATUS_O_WON;
                else if(sums[i] == 3)   gameStatus = STATUS_X_WON;
                else isGameOver = false;
            }
        }

        if(moveCount == 9 && !isGameOver) {
            gameStatus = STATUS_TIE;
            isGameOver = true;
        }

    }

    public static boolean isTurnX() {
        return moveCount % 2 == 1;
    }

    public static boolean isMyTurn() {
        return (isTurnX() && isMyMoveX()) || (!isTurnX() && !isMyMoveX());
    }
    public static boolean isMyMoveX() {
        return isMyMoveX;
    }
    public static boolean isGameOver() {
        return isGameOver;
    }
    public static void setMyMove(boolean isMyMoveX) {
        Game.isMyMoveX = isMyMoveX;
    }
    public static int getMoveCount() {
        return moveCount;
    }

    public static String getStatus() {
        return gameStatus;
    }

    public static void setStatus(String gameStatus) {
        Game.gameStatus = gameStatus;
    }

    public static void restartGame() {
        gameStatus = STATUS_NEW;
        moveCount = 1;
        isMyMoveX = false;
        isGameOver = false;
        for(int i = 0; i<3; i++)
            for(int j = 0; j<3; j++)
                board[i][j] = 0;
    }

    public static byte[][] getBoard() {
        return board;
    }

    public static void setBoard(byte[][] board) {
        Game.board = board;
        moveCount = 1;
        for(int i = 0; i<3; i++)
            for(int j = 0; j<3; j++)
                moveCount += Math.abs(board[i][j]);
        updateStatus();
    }
}
