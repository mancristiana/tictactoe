package dk.kea.swc.tictactoe.cristianaman;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    private static Stage primaryStage;
    private static GameController gameController = null;
    private static StartController startController = null;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Tic Tac Toe - Second Version");
        loadStart();
//        loadGame();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static void loadStart() {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(Main.class.getResource("view/start.fxml"));
            root = loader.load();
            Main.startController = loader.getController();
        } catch(Exception e) {
            e.printStackTrace();
        }
            primaryStage.setScene(new Scene(root, 320, 375));
            primaryStage.show();

    }

    public static void loadGame() {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader();
        try {
            loader.setLocation(Main.class.getResource("view/game.fxml"));
            root = loader.load();
            Main.gameController = loader.getController();
        } catch(Exception e) {
            e.printStackTrace();
        }
        primaryStage.setScene(new Scene(root, 340, 420));
        primaryStage.show();
    }

    public static GameController getGameController() {
        return gameController;
    }

    public static StartController getStartController() {
        return startController;
    }
}
