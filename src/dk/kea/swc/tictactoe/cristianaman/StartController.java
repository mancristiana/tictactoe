package dk.kea.swc.tictactoe.cristianaman;

import dk.kea.swc.tictactoe.cristianaman.tcpclient.Saturn;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

public class StartController implements Initializable {
    @FXML private HBox xoroBox;
    @FXML private HBox opponentNameBox;
    @FXML private HBox startBox;
    @FXML private HBox loadBox;

    @FXML private TextField youNameField;
    @FXML private TextField againstNameField;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        youNameField.setText(Saturn.getPlayerName());
        if(Saturn.isMyNameInServer())
            youNameField.setEditable(false);
        else  youNameField.setEditable(true);
        againstNameField.setText(Saturn.getOpponentName());
        xoroBox.setId("new_o");
        startBox.setId("start");
        loadBox.setId("load");
    }

    @FXML
    private void handleXorY() {
        if(xoroBox.getId().equals("new_x")) {
            xoroBox.setId("new_o");
            Game.setMyMove(false);
            opponentNameBox.setVisible(false);
            startBox.setId("start");
        }
        else {
            xoroBox.setId("new_x");
            Game.setMyMove(true);
            opponentNameBox.setVisible(true);
            startBox.setId("invite");

        }
     }

    @FXML
    private void handleStart() {
        if(isValid()) {
            youNameField.setEditable(false);
            Saturn.setPlayerName(youNameField.getText());
            Saturn.setOpponentName(againstNameField.getText());
            if(Game.isMyMoveX())
                Saturn.sendInvitation();
            else {
                Saturn.waitForInvitation();
            }
            // NO NEED TO DO THE  FOLLOWING AS IT IS DONE IN SATURN
//            Game.setStatus(Game.STATUS_STARTED);
//            Main.loadGame();
        }
    }

    private boolean isValid() {
        if(youNameField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "Upsie! You can't start the game with no name. \nPlease input your name, you rebel you!");
            return false;
        }
        if(xoroBox.getId().equals("new_x")) {
            if(againstNameField.getText().isEmpty()) {
                showAlert(Alert.AlertType.ERROR, "Upsie! You can't invite an opponent without name. \nPlease input your opponent's name, you rebel you!");
                return false;
            }
            if(againstNameField.getText().equals(youNameField.getText())) {
                showAlert(Alert.AlertType.ERROR, "Upsie! You can't invite yourself. \nPlease input another opponent's name, you rebel you!");
                return false;
            }
        }
        return true;
    }

    @FXML
    private void handleLoad() {
    }

    public static void showAlert(Alert.AlertType type, String content) {
        Alert alert = new Alert(type);
        alert.setTitle("Alert Alert Alert !!!");
        alert.setContentText(content);
        alert.showAndWait();
    }

    public TextField getYouNameField() {
        return youNameField;
    }
}
