package dk.kea.swc.tictactoe.cristianaman;

import dk.kea.swc.tictactoe.cristianaman.tcpclient.Saturn;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;

public class GameController implements Initializable {
    @FXML private GridPane boardGrid;
    @FXML private HBox saveBox;
    @FXML private Label detailsLabel;
    @FXML private Label xoLabel;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        saveBox.setId("save");
        customizeLabel();
        createBoard();
        resetBoard();
    }

    private void customizeLabel() {
        detailsLabel.setText(Saturn.getPlayerName() + " VS " + Saturn.getOpponentName() + "           ");
        if(Game.isMyMoveX()) {
            xoLabel.setText("x");
            xoLabel.setTextFill(Color.web("ae2829"));
        } else {
            xoLabel.setText("o");
            xoLabel.setTextFill(Color.web("51b62b"));
        }

    }

    private void createBoard() {
        for(int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++) {
                final int FINAL_X = x, FINAL_Y = y;
                HBox square = new HBox();
                square.setOnMouseClicked(event -> handleMove(FINAL_X, FINAL_Y, true));
                boardGrid.add(square, y, x); // Grid pane is reversed
            }
    }

    public void resetBoard() {
        for(int x = 0; x < 3; x++)
            for(int y = 0; y < 3; y++)
                getSquare(x, y).setId("blank");
    }

    public void handleMove(int x, int y, boolean isFromMyApp) {
        if(isFromMyApp == Game.isMyTurn()) {
            System.out.println(x + " " + y);
            HBox square = getSquare(x, y);
            if (square.getId().equals("blank")) {
                if (Game.isTurnX())
                    square.setId("x");
                else
                    square.setId("o");
                if (Game.isMyTurn()) {
                    Saturn.sendPosition(x, y);
                }
                Game.makeMove(x, y);
            } else {
                showAlert(Alert.AlertType.ERROR, "Really? That position is taken.");
            }
            if (!Game.getStatus().equals(Game.STATUS_STARTED)) {
                showAlert(Alert.AlertType.INFORMATION, Game.getStatus());
                handleNew();
            }
        }
    }

    public void handleNew() {
        resetBoard();
        Game.restartGame();
        Main.loadStart();
    }

    @FXML
    private void handleSave() {
        if(Game.isMyMoveX() && Game.isMyTurn()) {
            try {
                GameSaver.saveGame(Game.getBoard());
                handleNew();
            } catch (FileNotFoundException e) {
                showAlert(Alert.AlertType.ERROR, "Your game is not saved.\nCookie monster thought it was a cookie and ate it.");
            }
        } else {
            showAlert(Alert.AlertType.WARNING, "You need to wait for your turn and only player x can save the game. Sorry!");
        }

    }

    public void handleLoad() {
        byte[][] board = Game.getBoard();
        for(int i = 0; i<3; i++)
            for(int j = 0; j<3; j++) {
                HBox square = getSquare(i, j);
                if(board[i][j] == -1)       square.setId("o");
                else if(board[i][j] == 1)   square.setId("x");
                else                        square.setId("blank");
            }
    }

    public HBox getSquare(int x, int y) {
        return (HBox) boardGrid.getChildren().get(x * 3 + y + 1);
    }

    public static void showAlert(Alert.AlertType type, String content) {
        Alert alert = new Alert(type);
        alert.setTitle("Alert Alert Alert !!!");
        alert.setContentText(content);
        alert.showAndWait();
    }
}
